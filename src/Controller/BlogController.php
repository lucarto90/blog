<?php

namespace App\Controller;

use App\Entity\Blog;
use App\Form\BlogType;
use App\Repository\BlogRepository;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Scalar\MagicConst\File;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="blog_index")
     */
    public function indexAction()
    {
        return $this->render('blog/index.html.twig');
    }

    /**
     * @Route("/blog/{id?}", name="view_blog")
     */
    public function blogAction(BlogRepository $blogRepository, Blog $blog = null)
    {
        $listEntry = $blogRepository->findByOrder();
        $blog = $blog ?? current($listEntry);

        if (empty($listEntry)) {
            return $this->render('blog/notEntry.html.twig');
        }

        return $this->render('blog/view.html.twig',
            [
                'list' => $listEntry,
                'blog' => $blog,
            ]);

    }

    /**
     * @Route("/new", name="blog_new")
     */
    public function newEntryAction(Request $request, EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $form = $this->createForm(BlogType::class, $blog = new Blog());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form['file']->getData();

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('images_directory'), $fileName);

            $blog->setFile($fileName);
            $blog->setUser($user);

            $em->persist($blog);
            $em->flush();

            $this->addFlash(
                'info',
                'Nueva Entrada Creada!'
            );

            return $this->redirectToRoute('view_blog');
        }

        return $this->render('blog/newEntry.html.twig',
            [
                'form' => $form->createView(),
            ]);

    }

    /**
     * @Route("/edit/{id}", name="edit_entry")
     */
    public function editEntryAction(Request $request, Blog $blog, EntityManagerInterface $em)
    {
        $form = $this->createForm(BlogType::class, $blog);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            $file = $form['file']->getData();

            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('images_directory'), $fileName);

            $blog->setFile($fileName);

            $em->persist($blog);
            $em->flush();

            $this->addFlash(
                'info',
                'Entrada Editada!'
            );

            return $this->redirectToRoute('view_blog');
        }

        return $this->render('blog/newEntry.html.twig',
            [
                'form' => $form->createView(),
            ]);
    }
}
