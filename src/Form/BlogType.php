<?php

namespace App\Form;

use App\Entity\Blog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class,
                [
                    'label' => 'Titulo',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'titulo del articulo',
                        'class' => 'form-control',
                    ]
                ])
            ->add('file', FileType::class, [
                'label' => 'Imagen',
                "attr" => [
                    "class" => "form-control"
                    ],
                'data_class' => null,
            ])
            ->add('content',TextareaType::class,
                [
                    'label' => 'Contenido',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Contenido del articulo',
                        'class' => 'form-control',
                        'style' => 'height: 180px',
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Blog::class,
        ]);
    }
}
