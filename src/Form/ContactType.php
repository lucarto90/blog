<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,
                [
                    'label' => 'Nombre',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => 'Email',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('mensaje', TextareaType::class,
                [
                    'label' => 'Mensaje',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control',
                        'style' => 'height: 180px',
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
        ]);
    }
}
